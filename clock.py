#!/usr/bin/python3
# -*- coding: utf-8 -*-

from collections import namedtuple
from datetime import datetime, timedelta

from tkinter import *
from tkinter import ttk

class Orario:

    def __init__(self, root):

        root.title("Orario")

        root.tk.call("source", "Sun-Valley-ttk-theme-main/sv.tcl")
        root.tk.call("set_theme", "light")

        mainframe = ttk.Frame(root, padding="3 3 12 12")
        mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
        root.columnconfigure(0, weight=1)
        root.rowconfigure(0, weight=1)

        self.entrata = StringVar()
        entrata_entry = ttk.Entry(mainframe, width=7, textvariable=self.entrata)
        entrata_entry.grid(column=2, row=1, sticky=(W, E))

        self.inizio_pausa = StringVar()
        inizio_pausa_entry = ttk.Entry(mainframe, width=7, textvariable=self.inizio_pausa)
        inizio_pausa_entry.grid(column=2, row=2, sticky=(W, E))

        self.fine_pausa = StringVar()
        fine_pausa_entry = ttk.Entry(mainframe, width=7, textvariable=self.fine_pausa)
        fine_pausa_entry.grid(column=2, row=3, sticky=(W, E))

        ttk.Button(mainframe, text="Timbra! ⏰", command=self.clock).grid(column=2, row=4, sticky=W)

        ttk.Label(mainframe, text="Entrata").grid(column=1, row=1, sticky=E)
        ttk.Label(mainframe, text="Inizio pausa").grid(column=1, row=2, sticky=E)
        ttk.Label(mainframe, text="Fine pausa").grid(column=1, row=3, sticky=E)
        
        self.uscita_anticipata = StringVar()
        ttk.Label(mainframe, textvariable=self.uscita_anticipata).grid(column=1, row=5, columnspan=2, sticky=E)
        
        self.uscita_regolare = StringVar()
        ttk.Label(mainframe, textvariable=self.uscita_regolare).grid(column=1, row=6, columnspan=2, sticky=E)

        self.uscita_eccedenze = StringVar()
        ttk.Label(mainframe, textvariable=self.uscita_eccedenze).grid(column=1, row=7, columnspan=2, sticky=E)

        for child in mainframe.winfo_children(): 
            child.grid_configure(padx=5, pady=5)

        entrata_entry.focus()
        root.bind("<Return>", self.clock)

    def clock(self, *args):
        t_entrata = datetime.strptime(self.entrata.get(), "%H:%M")
        try:
            t_iniziopausa = datetime.strptime(self.inizio_pausa.get(), "%H:%M")
        except ValueError:
            t_iniziopausa = datetime.strptime("13:00", "%H:%M")
        try:
            t_finepausa = datetime.strptime(self.fine_pausa.get(), "%H:%M")
        except ValueError:
            t_finepausa = datetime.strptime("13:30", "%H:%M")

        d_orario_regolare = timedelta(0, 25920) # regular work hours 7 h 12 m
        d_orario_breve = timedelta(0, 21660) # short work hours 6 h 01 m
        d_pausa_minima = timedelta(0, 1800) # minimum lunch break 30 minutes

        d_pausa_effettiva = (t_finepausa - t_iniziopausa) if (t_finepausa - t_iniziopausa) >= d_pausa_minima else d_pausa_minima

        t_uscita_regolare = t_entrata + d_orario_regolare + d_pausa_effettiva
        t_uscita_anticipata = t_entrata + d_orario_breve + d_pausa_effettiva
        t_uscita_eccedenze = t_uscita_regolare + timedelta(0, 2400)

        self.uscita_anticipata.set("Uscita abbreviata 6:31 🕒 {}".format(t_uscita_anticipata.time()))
        self.uscita_regolare.set("Uscita regolare 7:42 🕓 {}".format(t_uscita_regolare.time()))
        self.uscita_eccedenze.set("Uscita eccedenze entro 🕔 {}".format(t_uscita_eccedenze.time()))

root = Tk()
Orario(root)
root.mainloop()